# Beeswarm

ES6 d3.js clustered circles along an axis visualization.

## Install

```bash
# install package
npm install @lgv/beeswarm
```

## Data Format

The following values are the expected input data structure; id can be omitted.

```json
[
    {
        "label": "xyz",
        "value": 1,
        "x": 5,
        "y": 2
    },
    {
        "label": "abc",
        "value": 3,
        "x": 10,
        "y": 8
    }
]
```

## Use Module

```bash
import { Beeswarm } from "@lgv/beeswarm";

// have some data
let data = [
    {
        "label": "xyz",
        "value": 1,
        "x": 5,
        "y": 2
    },
    {
        "label": "abc",
        "value": 3,
        "x": 10,
        "y": 8
    }
];

// initialize
const b = new Beeswarm(data);

// render visualization
b.render(document.body);
```

## Environment Variables

The following values can be set via environment or passed into the class.

| Name | Type | Description |
| :-- | :-- | :-- |
| `LGV_HEIGHT` | integer | height of artboard |
| `LGV_WIDTH` | integer | width of artboard |

## Events

The following events are dispatched from the svg element. Hover events toggle a class named `active` on the element.

| Target | Name | Event |
| :-- | :-- | :-- |
| bubble circle | `bubble-click` | on click |
| bubble circle | `bubble-mouseover` | on hover |
| bubble circle | `bubble-mousemout` | on un-hover |

## Style

Style is expected to be addressed via css. Any style not met by the visualization module is expected to be added by the importing component.

| Class | Element |
| :-- | :-- |
| `lgv-bubble` | circle element |
| `lgv-beeswarm` | top-level svg element |
| `lgv-node` | circle/text group element |
| `lgv-bubble-label` | circle text label element |
| `lgv-bubble-label-partial` | individual stacked lines of circle text label element |

## Actively Develop

```bash
# clone repository
git clone <repo_url>

# update directory context
cd beeswarm

# run docker container
docker run \
  --rm \
  -it  \
  -v $(pwd):/project \
  -w /project \
  -p 8080:8080 \
  node \
  bash

# FROM INSIDE RUNNING CONTAINER

# install module
npm install .

# run development server
npm run startdocker

# edit src/index.js
# add const b = new Beeswarm(data);
# add b.render(document.body);
# replace `data` with whatever data you want to develop with

# view visualization in browser at http://localhost:8080
```
