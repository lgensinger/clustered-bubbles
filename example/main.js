import packageJson from "../package.json";
import { Beeswarm } from "../src/index.js";
import { processEvent, renderDefault } from "@lgv/visualization-chart";

let data = [
    { label: "a", value: 1, x: 5, y: 2 },
    { label: "b", value: 2, x: 8, y: 3 },
    { label: "c", value: 4, x: 10, y: 1 },
    { label: "d", value: 1, x: 4, y: 2 },
    { label: "e", value: 3, x: 2, y: 5 },
    { label: "f", value: 10, x: 1, y: 4 }
];

// get elements
let container = document.getElementsByTagName("figure")[0];
let sourceContainer = document.getElementsByTagName("code")[0];
let outputContainer = document.getElementsByTagName("code")[1];

/**
 * Render initial visualization.
 * @param {element} container - DOM element
 * @param {array} data - object with key/value pairs of path
 */
function startup(data,container) {

    // update version in header
    let h1 = document.querySelector("h1");
    let title = h1.innerText;
    h1.innerHTML = `${title} <span>v${packageJson.version}</span>`;

    // render source data
    renderDefault(data,sourceContainer,outputContainer);

    // determine configs
    let width = container.offsetWidth;
    let height = width*0.75;
    let radiusExtent = [10,20];
    let xExtent = [0,null];
    let yExtent = [0,null];

    // initialize
    const b = new Beeswarm(data,width,height,null,radiusExtent,xExtent,yExtent);

    // render visualization
    b.render(container);

}

// load document
document.onload = startup(data,container);

// attach events
container.outputContainer = outputContainer;
container.addEventListener("bubble-click",processEvent);
container.addEventListener("bubble-mouseover", processEvent);
container.addEventListener("bubble-mouseout", processEvent);