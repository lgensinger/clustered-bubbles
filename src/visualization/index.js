import { ForceLayout as FL, ChartLabel, LinearGrid } from "@lgv/visualization-chart";
import { extent } from "d3-array";
import { axisBottom, axisLeft } from "d3-axis";
import { scaleLinear, scaleSqrt } from "d3-scale";
import { select } from "d3-selection";
import { transition } from "d3-transition";

import { configuration, configurationLayout } from "../configuration.js";

/**
 * Beeswarm is a sequential visualization with clustered values.
 * @param {array} data - objects where each represents a path in the hierarchy
 * @param {integer} height - artboard height
 * @param {array} radiusExtent - [min,max] of radii as floats
 * @param {integer} width - artboard width
 * @param {array} xExtent - [min,max] of x scale domain as floats
 * @param {array} yExtent - [min,max] of y scale domain as floats
 */
class Beeswarm extends LinearGrid {
    constructor(data, width=configurationLayout.width, height=configurationLayout.height, ForceLayout=null, radiusExtent=null, xExtent=null, yExtent=null, label=configuration.branding, name=configuration.name) {

        // initialize inheritance
        super(data, width, height, ForceLayout ? ForceLayout : new FL(data), label, name);

        // update self
        this.axisX = null;
        this.axisY = null;
        this.bubble = null;
        this.classBubble = `${label}-bubble`;
        this.classAxisX = `${label}-axis-x`;
        this.classAxisY = `${label}-axis-y`;
        this.classLabel = `${label}-label`;
        this.classLabelPartial = `${label}-label-partial`;
        this.classNode = `${label}-node`;
        this.label = null;
        this.labelPartial = null;
        this.node = null;
        this.radiusMax = radiusExtent && radiusExtent[1] ? radiusExtent[1] : this.unit * 2;
        this.radiusMin = radiusExtent && radiusExtent[0] ? radiusExtent[0] : this.unit * 0.25;
        this.ticksX = 5;
        this.ticksY = 5;
        this.xMax = xExtent && xExtent[1] !== null ? xExtent[1] : null;
        this.xMin = xExtent && xExtent[0] !== null ? xExtent[0] : null;
        this.yMax = yExtent && yExtent[1] !== null ? yExtent[1] : null;
        this.yMin = yExtent && yExtent[0] !== null ? yExtent[0] : null;

        // have to update layout functions after initialization
        this.Data.rScale = this.rScale;
        this.Data.xScale = this.xScale;
        this.Data.yScale = this.yScale;

    }

    /**
     * Construct axis.
     * @returns A d3 axis function.
     */
    get axisBottom() {
        return axisBottom(this.xScale)
            .ticks(this.ticksX);
    }

    /**
     * Construct axis.
     * @returns A d3 axis function.
     */
    get axisLeft() {
        return axisLeft(this.yScale)
            .ticks(this.ticksY);
    }

    /**
     * Determine x domain value.
     * @returns A float representing the artboard coordinate size.
     */
    get domainX() {
        return this.xScale.range()[1] - this.xScale.range()[0];
    }

    /**
     * Determine y domain value.
     * @returns A float representing the artboard coordinate size.
     */
    get domainY() {
        return this.yScale.range()[0] - this.yScale.range()[1];
    }

    /**
     * Determine artboard height.
     * @returns A float representing the height of the svg element.
     */
    get height() {
        return (this.heightSpecified - this.maximumSize) > this.radiusMax ? this.heightSpecified : this.heightSpecified + (this.maximumSize * 2);
    }

    /**
     * Determine largest possible bubble diameter.
     * @returns A d3 scale function.
     */
    get maximumSize() {
        return this.rScale.range()[1] * 2;
    }

    /**
     * Construct a radius scale.
     * @returns A d3 scale function.
     */
    get rScale() {
        return scaleSqrt()
            .domain(extent(this.data, d => this.Data.extractRadius(d)))
            .range([this.radiusMin, this.radiusMax]);
    }

    /**
     * Construct x scale.
     * @returns A d3 scale function.
     */
    get xScale() {
        let x = extent(this.data, d => this.Data.extractX(d));
        return scaleLinear()
            .domain([this.xMin !== null ? this.xMin : x[0], this.xMax !== null ? this.xMax : x[1]])
            .range([this.maximumSize, this.width - this.maximumSize]);
    }

    /**
     * Construct y scale.
     * @returns A d3 scale function.
     */
    get yScale() {
        let y = extent(this.data, d => this.Data.extractY(d));
        return scaleLinear()
            .domain([this.yMin !== null ? this.yMin : y[0], this.yMax !== null ? this.yMax : y[1]])
            .range([this.height - this.maximumSize, this.maximumSize]);
    }

    /**
     * Position and minimally style x-axis line in SVG dom element.
     */
    configureAxisX() {
        this.axisX
            .transition().duration(1000)
            .attr("class", this.classAxisX)
            .attr("stroke", "currentColor")
            .attr("transform", `translate(0,${this.yScale.range()[1] + (this.domainY / 2)})`)
            .call(this.axisBottom);
    }

    /**
     * Position and minimally style y-axis line in SVG dom element.
     */
    configureAxisY() {
        this.axisY
            .transition().duration(1000)
            .attr("class", this.classAxisY)
            .attr("stroke", "currentColor")
            .attr("transform", `translate(${this.xScale.range()[0] + (this.domainX / 2)},0)`)
            .call(this.axisLeft);
    }

    /**
     * Position and minimally style bubble shapes in SVG dom element.
     */
    configureBubbles() {
        this.bubble
            .transition().duration(1000)
            .attr("class", this.classBubble)
            .attr("data-label", d => this.Data.extractLabel(d))
            .attr("data-value", d => this.Data.extractValue(d))
            .attr("fill", "lightgrey")
            .attr("r", d => this.rScale(this.Data.extractRadius(d)));
    }

    /**
     * Position and minimally style labels in SVG dom element.
     */
    configureLabels() {
        this.label
            .attr("class", this.classLabel)
            .attr("data-label", d => this.Data.extractLabel(d))
            .attr("data-value", d => this.Data.extractValue(d))
            .attr("text-anchor", "middle")
            .attr("pointer-events", "none")
            .attr("x", 0)
            .attr("y", 0);
    }

    /**
     * Position and minimally style label partials in SVG dom element.
     */
    configureLabelPartials() {
        this.labelPartial
            .transition().duration(300)
            .attr("class", this.classLabelPartial)
            .attr("x", 0)
            .attr("dy", (d,i) => this.Label.stackIsTooTall(d, this.rScale(this.Data.extractRadius(d)) * 2) ? `${this.Label.determineHeight(this.Data.extractLabel(d)) * 0.28}px` : this.Label.calculateDy(i, d, this.rScale(this.Data.extractRadius(d)) * 2))
            .attr("visibility", (d,i) => i == 0 ? (this.Label.singleIsTooTall(d, this.rScale(this.Data.extractRadius(d)) * 2) ? "hidden" : "visible") : (!this.Label.stackIsTooTall(d, this.rScale(this.Data.extractRadius(d)) * 2) ? "visible" : "hidden"))
            .textTween((d,i) => this.tweenText(this.Data.extractLabel(d), this.Data.extractValue(d), this.rScale(this.Data.extractRadius(d)), i));
    }

    /**
     * Position and minimally style bubbles in SVG dom element.
     */
    configureBubbleEvents() {
        this.bubble
            .on("click", (e,d) => this.configureEvent("bubble-click",d,e))
            .on("mouseover", (e,d) => {

                // update class
                select(e.target).attr("class", `${this.classBubble} active`);

                // send event to parent
                this.configureEvent("bubble-mouseover",d,e);

            })
            .on("mouseout", (e,d) => {

                // update class
                select(e.target).attr("class", this.classBubble);

                // send event to parent
                this.artboard.dispatch("bubble-mouseout", {
                    bubbles: true
                });

            });
    }

    /**
     * Position and minimally style nodes in SVG dom element.
     */
    configureNodes() {
        this.node
            .transition().duration(1000)
            .attr("class", this.classNode)
            .attr("data-id", d => this.Data.extractId(d))
            .attr("data-label", d => this.Data.extractLabel(d));
    }

    /**
     * Construct x-axis.
     * @param {node} domNode - d3.js selection
     * @returns A d3.js selection.
     */
    generateAxisX(domNode) {
        return domNode
            .selectAll(`.${this.classAxisX}`)
            .data(d => [d])
            .join(
                enter => enter.append("g"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Construct y-axis.
     * @param {node} domNode - d3.js selection
     * @returns A d3.js selection.
     */
    generateAxisY(domNode) {
        return domNode
            .selectAll(`.${this.classAxisY}`)
            .data(d => [d])
            .join(
                enter => enter.append("g"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Generate bubble shapes in SVG element.
     * @param {node} domNode - d3.js SVG selection
     */
    generateBubbles(domNode) {
        return domNode
            .selectAll(`.${this.classBubble}`)
            .data(d => [d])
            .join(
                enter => enter.append("circle"),
                update => update,
                exit => exit.remove().transition().duration(1000)
            );
    }

    /**
     * Generate bubble labels in SVG element.
     * @param {node} domNode - d3.js SVG selection
     */
    generateLabels(domNode) {
        return domNode
            .selectAll(`.${this.classLabel}`)
            .data(d => [d])
            .join(
                enter => enter.append("text"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Generate bubble label partials in SVG element.
     * @param {node} domNode - d3.js SVG selection
     */
    generateLabelPartials(domNode) {
        return domNode
            .selectAll(`.${this.classLabelPartial}`)
            .data(d => [d, d])
            .join(
                enter => enter.append("tspan"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Construct bubble group in HTML DOM.
     * @param {selection} domNode - d3 selection
     * @returns A d3.js selection.
     */
    generateNodes(domNode) {
        return domNode
            .selectAll(`.${this.classNode}`)
            .data(this.data)
            .join(
                enter => enter.append("g"),
                update => update,
                exit => exit.transition()
                    .duration(1000)
                    .attr("transform", `translate(${this.width / 2},${this.height / 2}) scale(0)`)
                    .remove()
            );
    }

    /**
     * Generate visualization.
     */
    generateChart() {

        // initialize labeling
        this.Label = new ChartLabel(this.artboard, this.Data);

        // generate group for each bubble
        this.node = this.generateNodes(this.content);
        this.configureNodes();

        // generate bubbles
        this.bubble = this.generateBubbles(this.node);
        this.configureBubbles();
        this.configureBubbleEvents();

        // simulate force to generate static positions of bubbles
        for (var i = 0; i < 360; ++i) this.Data.layout.tick();

        // update node positions
        this.node
            .transition().duration(1000)
            .attr("transform", d => `translate(${d.x},${d.y})`);

        // generate axes
        this.axisX = this.generateAxisX(this.content);
        this.configureAxisX();
        this.axisY = this.generateAxisY(this.content);
        this.configureAxisY();

        // generate labels
        this.label = this.generateLabels(this.node);
        this.configureLabels();

        // generate label partials so they stack
        this.labelPartial = this.generateLabelPartials(this.label);
        this.configureLabelPartials();

    }

};

export { Beeswarm };
export default Beeswarm;
